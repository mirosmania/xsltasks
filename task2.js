/*задание 2*/
function makeQueryStringFromObject(object){


    var questionMarkPos=randomUrl.lastIndexOf('?');

    //а есть ли в url вопросительный знак?
    //TODO по хорошему проверить на валидность querystring,а не только на то, явл-ся ли вопросительный знак последнимм символом в строке
    if(questionMarkPos >= 0){
        return getUrlFromObject(questionMarkPos!=(randomUrl.length-1));//если вопросительный знак последний символ в URL то аргумент==false
    }
    randomUrl+='?';
    return getUrlFromObject(false);

    /**
     * Из объекта возвращает строку вида 'key1=value1&key2=value2...'
     *
     * @returns {string}
     */
    function prepareParamsForQueryString(){
        var paramsForQueryString='';
        for (var key in object) {
            paramsForQueryString+=key+'='+object[key]+'&';
        }
        return paramsForQueryString.substring(0, paramsForQueryString.length - 1);
    }

    /**
     * если в аргументе передано false,значит вопросительный знак явл-ся последним символом в randomUrl,
     * тогда  первый символ кверистринг будет без '&'
     *
     * @param bool
     * @returns {string}
     */
    function getUrlFromObject(bool){
        return (bool)?randomUrl+'&'+prepareParamsForQueryString():randomUrl+prepareParamsForQueryString();
    }
}



var randomUrl='ya.ru';

var p =
{
    "p1": "value1",
    "p2": "value2",
    "p3": "value3"
};

makeQueryStringFromObject(p);
